package com.myntra.bootcamp.servlet.handlers;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class UserHandler {

	public static void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<body>");
        out.println("<h1>This is Get Method for User</h1>");
        out.println("</body>");
        out.println("</html>"); 
    }
    
    public static void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException{
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<body>");
        out.println("<h1>This is Post Method for User</h1>");
        out.println("</body>");
        out.println("</html>"); 
    }
    
    public static void doPut(HttpServletRequest request, HttpServletResponse response) throws IOException{
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<body>");
        out.println("<h1>This is Put Method for User</h1>");
        out.println("</body>");
        out.println("</html>"); 
    }

    public static void doDelete(HttpServletRequest request, HttpServletResponse response) throws IOException{
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<body>");
        out.println("<h1>This is Delete Method for User</h1>");
        out.println("</body>");
        out.println("</html>"); 
    }
    
}
